const people = [
  {
    email: 'kyle@gocleary.com',
    firstName: 'Kyle',
    id: '4069',
    lastName: 'Borchardt',
    manager: true,
    numMeetings: 10,
    title: 'Head of Employee Experience',
    profilePhoto: 'https://randomuser.me/api/portraits/lego/3.jpg',
    kudos: []
  },
  {
    email: 'loyd.hintz@gocleary.com',
    firstName: 'Loyd',
    id: '4070',
    lastName: 'Hintz',
    manager: false,
    numMeetings: 5,
    title: 'National Configuration Associate',
    profilePhoto: 'https://randomuser.me/api/portraits/lego/0.jpg',
    kudos: []
  },
  {
    email: 'jherman@gocleary.com',
    firstName: 'Joan',
    id: '4071',
    lastName: 'Herman',
    manager: false,
    numMeetings: 8,
    title: 'Senior Security Officer',
    profilePhoto: 'https://randomuser.me/api/portraits/lego/6.jpg',
    kudos: []
  },
  {
    email: 'b.bergs@gocleary.com',
    firstName: 'Barton',
    id: '4072',
    lastName: 'Bergs',
    manager: false,
    numMeetings: 2,
    title: 'Customer Infrastructure Associate',
    profilePhoto: 'https://randomuser.me/api/portraits/lego/2.jpg',
    kudos: []
  },
  {
    email: 'jbarton@gocleary.com',
    firstName: 'Jonathan',
    id: '4073',
    lastName: 'Barton',
    manager: false,
    numMeetings: 6,
    title: 'Customer Creative Engineer',
    profilePhoto: 'https://randomuser.me/api/portraits/lego/4.jpg',
    kudos: []
  },

  {
    email: 'ceslock@gocleary.com',
    firstName: 'Cesar',
    id: '4081',
    lastName: 'Lockman',
    manager: false,
    numMeetings: 3,
    title: 'Software Engineer',
    profilePhoto: 'https://randomuser.me/api/portraits/lego/1.jpg',
    kudos: []
  }
]

export default people
