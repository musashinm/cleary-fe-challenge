To begin run `yarn` to install all the necessary dependencies.

After this you can run `yarn start` to start webpack's dev server.

At this point you should see a loading spinner and a toggle button in the top left corner of the screen when you visit http://localhost:8080/

![picture](img/home.png)

There are two components and test data available to use:

- The `<Spinner />` component, which you need to use when displaying the list of users.

- The `<Toggle />` component, the UI is ready, but you need to setup state management/event handling in order the switch themes.

- List of users in `data/people`

- The `<App />` imports `<Spinner />`, `<Toggle />`, the list of users, and a basic stylesheet. You can start in this file. Ignore the the `react-hot-loader` and the `hot` higher order component,
  this is used to speed up development.


The idea is you build a simple page that:

- Has a theme switcher (you'll use `<Toggle />` for this) that has two themes, Light (#dee4e7) and Dark (#37474f).
- A list of users with a loading indicator faking a server response wait time.
- Each user should have a call to action, *Send Kudos*. When clicked, a modal with a text area should appear. In the modal,
  the person using the application can send kudos to that user and the text value will now appear on the user card.
- Implementation is up to you, but make sure you write clean and readable code. You can use react-redux, simple state, or the new context api.

Considerations:

This setup uses babel's "transform-class-properties" plugin, this allows us to easily bind this to class methods using fat arrows:

```javascript
imBoundToThis = () => {
  console.log(this)
}
```

[Video Instructions](https://drive.google.com/file/d/13z7PTMlbRNzs-4Hx8nryrb2JX7bMFxdV/view)
