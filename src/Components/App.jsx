import React, { Component } from 'react'

import classNames from 'classnames'

import { hot } from 'react-hot-loader/root'

import Footer from 'Components/Footer'
import Header from 'Components/Header'
import Modal from 'Components/Modal'
import MainContent from 'Components/MainContent'

import people from 'Data/people'

import './App.css'

class App extends Component {
  imBoundToThis = () => {
    console.log(this) // eslint-disable-line no-console
  }

  constructor(props) {
    super(props)

    this.state = {
      data: { loading: true, people: [] },
      modal: { opened: false, personId: null },
      theme: 'dark'
    }

    this.closeModal = this.closeModal.bind(this)
    this.openModal = this.openModal.bind(this)
    this.switchTheme = this.switchTheme.bind(this)
    this.updatePersonKudos = this.updatePersonKudos.bind(this)
  }

  componentDidMount() {
    setTimeout(() => this.setState({ data: { loading: false, people } }), 2500)
  }

  closeModal(e) {
    e.preventDefault()
    this.setState({ modal: { opened: false, personId: null } })
  }

  openModal(e, personId) {
    e.preventDefault()
    this.setState({ modal: { opened: true, personId } })
  }

  switchTheme() {
    this.setState({ theme: this.state.theme === 'light' ? 'dark' : 'light' })
  }

  updatePersonKudos(id, kudo) {
    const {
      data: { loading, people: peopleList }
    } = this.state

    peopleList.forEach((person) => {
      if (person.id === id) person.kudos.push(kudo)
    })

    this.setState({ data: { loading, people: peopleList } })
  }

  render() {
    const {
      data: { loading, people: peopleList },
      modal,
      theme
    } = this.state

    return (
      <div>
        <div className={classNames('App', `${theme}-theme`)}>
          <Header />
          <MainContent loading={loading} openModal={this.openModal} people={peopleList} />
          <Footer switchTheme={this.switchTheme} />
        </div>
        {modal.opened && (
          <Modal closeModal={this.closeModal} personId={modal.personId} updatePersonKudos={this.updatePersonKudos} />
        )}
      </div>
    )
  }
}

export default hot(App)
