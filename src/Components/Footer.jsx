import React from 'react'
import PropTypes from 'prop-types'

import Toggle from 'Components/common/toggle'

import './Footer.css'

const Footer = ({ switchTheme }) => (
  <div className="Footer">
    <Toggle switchTheme={switchTheme} />
  </div>
)

Footer.propTypes = {
  switchTheme: PropTypes.func
}

export default Footer
