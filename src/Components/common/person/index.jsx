import React from 'react'
import PropTypes from 'prop-types'

import Kudo from './Kudo'

import './Person.css'

const Person = ({ data, openModal }) => {
  const { email, firstName, id, kudos, lastName, profilePhoto, title } = data

  if (kudos.length > 0) return <Kudo data={data} />

  return (
    <div className="Person">
      <div className="personal-info">
        <img src={profilePhoto} />
        <p>
          {`${firstName} ${lastName}`} | <a href={`mailto: ${email}`}>{email}</a>
        </p>
        <p>
          <strong>{title}</strong>
        </p>
      </div>
      <div className="options">
        <button type="button" onClick={(e) => openModal(e, id)}>
          Give Kudo
        </button>
      </div>
    </div>
  )
}

Person.propTypes = {
  data: PropTypes.shape({
    email: PropTypes.string,
    firstName: PropTypes.string,
    id: PropTypes.string,
    kudos: PropTypes.arrayOf(PropTypes.string),
    lastName: PropTypes.string,
    profilePhoto: PropTypes.string,
    title: PropTypes.string
  }),
  openModal: PropTypes.func
}

export default Person
