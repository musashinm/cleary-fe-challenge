import React from 'react'
import PropTypes from 'prop-types'

import './Kudo.css'

const Kudo = ({ data: { firstName, kudos, lastName, profilePhoto } }) => (
  <div className="Kudo">
    <div className="message">
      <strong>{`${firstName} ${lastName}`}</strong> got some kudos!
    </div>
    <div className="avatar">
      <img src={profilePhoto} />
    </div>
    <div className="kudos">
      {kudos.map((kudo, i) => (
        <p key={i}>
          <em>&quot;{kudo}&quot;</em>
        </p>
      ))}
    </div>
  </div>
)

Kudo.propTypes = {
  data: PropTypes.shape({
    firstName: PropTypes.string,
    kudos: PropTypes.arrayOf(PropTypes.string),
    lastName: PropTypes.string,
    profilePhoto: PropTypes.string
  })
}

export default Kudo
