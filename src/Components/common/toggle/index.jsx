import React from 'react'
import PropTypes from 'prop-types'

import './Toggle.css'

const Toggle = ({ switchTheme }) => (
  <label className="Switch">
    <input type="checkbox" onChange={switchTheme} />
    <span className="slider round" />
  </label>
)

Toggle.propTypes = {
  switchTheme: PropTypes.func
}

export default Toggle
