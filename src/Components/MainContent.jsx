import React from 'react'
import PropTypes from 'prop-types'

import Person from 'Components/common/person'
import Spinner from 'Components/common/spinner'

import './MainContent.css'

const MainContent = ({ loading, openModal, people }) => (
  <div className="MainContent">
    {loading ? (
      <Spinner />
    ) : (
      <ul>
        {people.map((person, i) => (
          <li key={i}>
            <Person data={person} openModal={openModal} />
          </li>
        ))}
      </ul>
    )}
  </div>
)

MainContent.propTypes = {
  loading: PropTypes.bool,
  openModal: PropTypes.func,
  people: PropTypes.arrayOf(PropTypes.object)
}

export default MainContent
