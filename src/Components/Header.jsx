import React from 'react'

import './Header.css'

const Header = () => (
  <div className="Header">
    <h2>Hello Cleary</h2>
  </div>
)

export default Header
