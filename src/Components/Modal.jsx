import React from 'react'
import PropTypes from 'prop-types'

import './Modal.css'

const Modal = ({ closeModal, personId, updatePersonKudos }) => {
  const handleSubmit = (e) => {
    const { personId, kudo } = e.target.elements

    e.preventDefault()
    updatePersonKudos(personId.value, kudo.value)
    closeModal(e)
  }

  return (
    <div className="Modal">
      <form onSubmit={(e) => handleSubmit(e)}>
        <input type="hidden" name="personId" value={personId} />
        <div className="content">
          <div className="close">
            <a href="#" onClick={(e) => closeModal(e)}>
              X
            </a>
          </div>
          <div className="inputs">
            <textarea name="kudo" placeholder="Write some kudo..." rows="5" />
          </div>
          <div className="actions">
            <button type="button" onClick={(e) => closeModal(e)}>
              Cancel
            </button>
            <button type="submit">Submit</button>
          </div>
        </div>
      </form>
    </div>
  )
}

Modal.propTypes = {
  closeModal: PropTypes.func,
  personId: PropTypes.string,
  updatePersonKudos: PropTypes.func
}

export default Modal
